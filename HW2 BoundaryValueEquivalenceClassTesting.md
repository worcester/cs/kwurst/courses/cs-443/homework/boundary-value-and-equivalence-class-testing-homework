# Homework 2 &mdash; Boundary Value and Equivalence Class Testing

## Objective

Practice identifying test cases using Boundary Value Testing, Equivalence
Class Testing, and Edge Testing.

## Instructions

For the questions below, use the following function:

`int discountPercent(double price, int pastOrderCount)`

returns the percent discount to be given, when given the price of the merchandise, and the number of orders the customer has placed in the past.

Past Order Count | Percent Discount
--- | ---
pastOrderCount < 10 | 0%
10 ≤ pastOrderCount ≤ 40 | 5%
40 < pastOrderCount | 10%

Orders over $1000.00 are given an automatic, additional 1% discount.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Ranges

1. List the valid range of values for each of the two variables. Give the minimum and maximum, and whether each extreme is a "physical" limit or an arbitrary limit. If it's an arbitrary limit, why did you choose that limit?

    1. `price`
    2. `pastOrderCount`

### Normal Boundary Values

1. List the 5 *normal boundary values* for each of the two variables.

    1. `price`
    2. `pastOrderCount`

### Unique Test Cases

1. Give the set of unique test cases for the *normal boundary values* in set notation.
2. Give the test cases in a table format including the expected output for each case.

### Robust Boundary Values

1. List the additional *robust boundary values*.
2. List the additional test cases for the *robust boundary values* in table format.

### Equivalence Classes

1. What are the equivalence class boundaries in each of the two variables?

    1. `price`
    2. `pastOrderCount`

2. List the intervals within the boundaries in interval notation.
3. List the equivalence classes for valid values in set notation.
4. List the equivalence classes for invalid values in set notation.

### Weak Normal Equivalence Class Test Cases

1. List test cases for Weak Normal Equivalence Classes in table format. Number them.
2. Do you see any gaps or problems? If so, what are they?

### Strong Normal Equivalence Class Test Cases

1. List additional test cases for Strong Normal Equivalence Classes in table format. Number them.
2. Do you see any gaps or problems? If so, what are they?

## *Intermediate Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Worst-Case Boundary Values

1. List 5 additional *worst-case boundary values* as pairs.
2. How many additional cases should there be (in total) for the worst-case.
3. List 5 additional *robust worst-case boundary values*.
4. How many additional cases should there be (in total) for the robust worst-case.

### Edge Testing

1. List the normal and robust edge values for both variables in the form given in Activity 7 Model 3.

## *Advanced Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Advanced Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Weak Robust Equivalence Class Test Cases

1. List additional test cases for Weak Robust Equivalence Classes in table format.
2. Which cases (by number) from above are included?
3. Do you see any gaps or problems? If so, what are they?

### Strong Robust Equivalence Class Test Cases

1. List 4 additional test cases for Strong Robust Equivalence Classes in table format.
2. Do you see any gaps or problems? If so, what are they?

## Specification

You may write your answers using any program that you wish.
You should choose a program that allows you to create tables
and write with subscripts. ***You must save your answers as a
PDF file and upload that PDF to the HW2 Assignment in
Blackboard.***

To be considered Acceptable your assignment must:

* Be submitted in PDF format.
* Your ranges must be given in the format in Activity 6: a &le; x<sub>1</sub> &le; b
* You must label your boundary values as in Activity 6: x<sub>1min</sub>, etc.
* You must list your boundary value pairs as in Activity 6: &lt;value, value&gt; specifying the ordering of the variables in the pair.
* You must give your test cases in a table with the same columns as in Activity 6 Model 3.
* You must use the interval notation as in Acctivity 7: [a, b), etc.
* You must use the set notation as in Activity 7: V1 = {x<sub>1</sub>: a &le; x<sub>1</sub> &lt; b} 
* You must answer all the questions given under each section in addition to the values asked for.

## Due Date

21 February 2024 - 23:59

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.